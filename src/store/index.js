import { createStore } from 'vuex'

export default createStore({
  state: {
    todos: [
      {id: 1, task: "Task One", show: true},
      {id: 2, task: "Task Two", show: true},
      {id: 3, task: "Task Three", show: true},
      {id: 4, task: "Task Four", show: true}
    ]
  },
  mutations: {
    ADD_TODO(state, item){
      state.todos.push({id: state.todos.length+1, task: item, show:true})
    },

    DELETE_TODO(state, todo){
      let index = state.todos.indexOf(todo)
      state.todos.splice(index,1);
    },

    TOGGLE_TODO(state, todo){
      todo.show = !todo.show
    }
  },
  actions: {
    addTodo({commit}, todo){
      commit('ADD_TODO', todo)
    },

    deleteTodo({commit}, todo){
      commit('DELETE_TODO', todo)
    },

    toggleTodo({commit}, todo){
      commit('TOGGLE_TODO', todo)
    }
  },
  getters:{
    totalTodos(state){
      return state.todos.length
    },

    activeTodos(state){
      return state.todos.filter((todo) => {return todo.show === true}).length
    }
  },
  modules: {
  }
})
